
import java.util.ArrayList;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

/**
 *
 * @author Dáire
 */
public class Regression {
    SimpleRegression r = new SimpleRegression();
    public double[] getLine(ArrayList<Double> x,ArrayList<Double> y) 
    {
        
        double intercept = r.getIntercept();
        double slope = r.getSlope();
        double[] line = new double[2];
        line[0]=intercept;
        line[1]=slope;
        return line;
        
    }
    
    public double getCorrelation(double[] x, double[] y){
        PearsonsCorrelation p = new PearsonsCorrelation();
       double  r=p.correlation(x,y);
        return r;
    }
}
